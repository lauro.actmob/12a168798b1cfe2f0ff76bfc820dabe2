jQuery('document').ready(function(){

    let wWindow = jQuery('body').width()

    if (wWindow <= 764) {
        jQuery('#navbar').hide()
    }

    jQuery('.btn-menu').click(function(){
        jQuery('#navbar').animate({
            height: 'toggle'
        });
    })

    //Starting the Slick Slide (Carrossel)
    if (wWindow <= 764) {
        jQuery('.slide-testimonials').slick({
            // infinite: true,           
            //asNavFor: '.slider-miniatures',
            responsive: [{
                breakpoint: 993,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                    arrows: false,
                    autoplay: true,
                    autoplaySpeed: 2500,
                }
            }]
        });
    }

})